class Character {
    constructor(name, role, armor, dmg) {
        this.name = name;
        this.role = role;
        this.armor = 0;
        this.dmg = 0;
    }

    armour() {
        console.log(`${this.name} has got ${this.armor} armors`);
    }
    damage() {
        console.log(`${this.name} has got ${this.dmg} dmgs`);
        if (role == 'mage' || role == 'support') {
            console.log(`${this.dmg} is the 'magic damage'`);
        } else {
            console.log(`${this.dmg} is the 'attack damage'`);
        }
    }
}

//class Mage
class Mage extends Character {
    constructor(name, role, armor, dmg, magicPowers) {
        super(name, role, armor, dmg);
        this.magicPowers = magicPowers;
    }

    fly() {
        console.log(`${this.name} can fly as well`);
    }
}



//class Support 
class Support extends Mage {
    constructor(name, role, armor, dmg, magicPowers, heals) {
        super(name, role, armor, dmg, magicPowers);
        this.heals = heals;
    }
    healing() {
        console.log(`${this.name} can heal ${this.heals} things`);
    }
}



//class adc
class Adc extends Character {
    constructor(name, role, armor, dmg, attackdamages) {
        super(name, role, armor, dmg);
        this.attackdamages = attackdamages;
        //this.range = 0;
    }
    Adcrange(range) {
        if (range < 30) {
            console.log(`${this.range} is a close range`);
        }
    }
}

let Mar = new Character('Maree', 'Support', 5, 19);
let Gang = new Character('Gango', 'Assassin', 1, 32);
let Nat = new Character('Natia', 'Adc', 25, 1);
let Geo = new Mage('George', 'Mage', 2, 34, 1);
let David = new Support('David', 'Support', 2, 12, 5, 300);
let Bob = new Adc('Bobito', 'adc', 2, 12, 9);

