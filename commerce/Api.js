import { SERVER__ADDRESS } from "./Config.js";
const callfunc = async (url) => {
    const req = await fetch(SERVER__ADDRESS + url);
    const result = await req.json();
    return result;
}

export const getAllProducts = async (sort = null) => {
    return await callfunc(`products${sort ? `?sort=${sort}` : ''}`);
}


/*export const getAllCategories = async () => {
    const res = await callfunction('products/categories');
    return res;
}*/