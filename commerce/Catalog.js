import { getAllProducts } from "./Api.js";
import { generateProductsHTML } from "./Product.js";


const catalog = document.getElementsByClassName('catalog')[0];

const FillingUpCatalog = async (sort = null) => {
    const productList = await getAllProducts(sort);
    catalog.innerHTML = generateProductsHTML(productList);
};

FillingUpCatalog();

//sorting
const sort = document.getElementById('sort');
sort.addEventListener("change", () => {
    FillingUpCatalog(sort.value);
});

//searching 
const searchquery = document.getElementById('searchquery');
const searchbutton = document.getElementById('searchbutton');
searchbutton.addEventListener("click", () => {
    getAllProducts(sort).then(result => {
        const filtereditems = result.filter((product) => product.title.toLowerCase().indexOf(searchquery.value) != -1);
        catalog.innerHTML = generateProductsHTML(filtereditems);
    });
});

