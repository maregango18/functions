

export const generateproducts = (product) => `
            <div class="catalog__product">
                <div  class="catalog__photo">
                    <img src="${product.image}"/>
                </div>
                <div class="catalog__title">
                     ${product.title}
                </div>
                <div class="catalog__prices">
                    ${product.price}$
                </div>
            </div>
  
            `;

export const generateProductsHTML = (productList) => {
    let productHTML = "";
    for (const product of productList) {
        productHTML += generateproducts(product);
    }
    return productHTML;
}