let PowerRangers1 = [44, 23, 71];
let PowerRangers2 = [85, 54, 41];
let FairyTales1 = [65, 54, 49];
let FairyTales2 = [23, 34, 47];
//1.1 calcaverage
function calcAverage(arr) {
    let i = 0, sum = 0, len = arr.length;
    while (i < len) {
        sum = sum + arr[i++];
    }
    return sum / len;
}
//function calcAverage(arr){
return arr.reduce((a, b) => (a + b)) / arr.length;
}
//1.2 
let PR1 = calcAverage(PowerRangers1);
let FT1 = calcAverage(FairyTales1);
let PR2 = calcAverage(PowerRangers2);
let FT2 = calcAverage(FairyTales2);
console.log(PR1, FT1, PR2, FT2);
//1.3 funct checkwinner
function checkWinner(a, b) {
    if (a > b * 2) {
        console.log(a, 'PR is the winner');
    } else if (b > a * 2) {
        console.log(b, 'FT is the winner');
    } else if ((a == b * 2) || (b == a * 2)) {
        console.log('tie');
    } else {
        console.log('Curling is not your bag');
    }
}
1.4
let result1 = checkWinner(PR1, FT1);
let result2 = checkWinner(PR2, FT2);

//Tip calculator
//1.1 func calcTip
function calcTip(x) {
    if (x > 50 && x < 300) {
        return (x * 15) / 100;
    } else {
        return (x * 20) / 100;
    }
}
//console.log(calcTip(60));
//2.2 array
let checks = [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52];
//2.3 tiparr
let arrTip = checks.map(calcTip);
console.log(checks, arrTip);
//2.4 
let sumcosts = checks.map(function (item, index) {
    return item + arrTip[index];
});
console.log(sumcosts);
//2.6 avg
let avgtip = calcAverage(arrTip);
let avgsumcosts = calcAverage(sumcosts);
console.log(avgtip, avgsumcosts);






