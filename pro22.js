import { txt } from "./txt.js";

/*const url = 'https://gitlab.com/maregango18/functions/-/blob/master/p022_names.txt';

/*const response = await fetch(url, { mode: "no-cors", credentials: "same-origin" });
const myData = await response;
console.log(myData);
async function postData(url = '', data = {}) {
    const response = await fetch(url, { mode: 'no-cors' });
    return response.json();
}
postData(url, { mode: 'no-cors' })
    .then(data => {
        console.log(data);
    });*/

function alphaValue(s) {
    let sum = 0;
    for (let i = 0; i < s.length; i++) {
        sum += s.charCodeAt(i) - 64;
    }
    return sum;
}

function sorting(arr) {
    let sorted = arr.sort();
    let total = 0;
    for (let i = 0; i < sorted.length; i++) {
        total += alphaValue(sorted[i]) * (i + 1);
    }
    return total;
}

console.log(sorting(txt));